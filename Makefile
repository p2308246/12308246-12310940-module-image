all: test affichage exemple

test : ./obj/Pixel.o ./obj/Image.o ./obj/mainTest.o
	g++ -ggdb ./obj/Pixel.o ./obj/Image.o ./obj/mainTest.o -o test
	mv test ./bin/

affichage : ./obj/Pixel.o ./obj/Image.o ./obj/ImageViewer.o ./obj/mainAffichage.o
	g++ -ggdb ./obj/Pixel.o ./obj/Image.o ./obj/ImageViewer.o ./obj/mainAffichage.o -o affichage -lSDL2 -lSDL2_image
	mv affichage ./bin/

exemple : ./obj/Pixel.o ./obj/Image.o ./obj/mainExemple.o
	g++ -ggdb ./obj/Pixel.o ./obj/Image.o ./obj/mainExemple.o -o exemple 
	mv exemple ./bin/

./obj/mainTest.o : ./src/Pixel.h ./src/Image.h ./src/mainTest.cpp
	g++ -ggdb -c ./src/mainTest.cpp
	mv mainTest.o ./obj/

./obj/mainAffichage.o : ./src/Pixel.h ./src/Image.h ./src/mainAffichage.cpp
	g++ -ggdb -lSDL2 -c ./src/mainAffichage.cpp
	mv mainAffichage.o ./obj/

./obj/mainExemple.o : ./src/Pixel.h ./src/Image.h ./src/mainExemple.cpp
	g++ -ggdb -c ./src/mainExemple.cpp
	mv mainExemple.o ./obj/

./obj/Image.o : ./src/Pixel.h ./src/Image.h ./src/Image.cpp
	g++ -ggdb -lSDL2 ./src/Pixel.h ./src/Image.h -c ./src/Image.cpp
	mv Image.o ./obj/

./obj/Pixel.o : ./src/Pixel.cpp ./src/Pixel.h
	g++ -ggdb -c ./src/Pixel.cpp
	mv Pixel.o ./obj/

./obj/ImageViewer.o : ./src/Image.h ./src/ImageViewer.cpp ./src/ImageViewer.h
	g++ -ggdb -lSDL2 ./src/Image.h -c ./src/ImageViewer.cpp
	mv ImageViewer.o ./obj/

doc : doc/doxyfile
	doxygen doc/doxyfile

clean :
	-rm -f obj/*.o 
	-rm -f src/*.gch
	-rm -f data/*.ppm
	-rm -f bin/*