# L2_CDA_ModuleImage p2308246/p2310940

Module Image de LIFAPCD de COASNE Jules et ALOUI Chahd.

## Compilation

Pour compiler, utiliser la commande `make` dans le répertoire.

## Module

Le module image permet la création et l'affichage d'image primitive
donnée par une dimension et un tableau d'instance de la classe Pixel.

## Exécutable

### test

Teste de régression sur le module Image.

### exemple

Créé, sauvegarde et charge dans instance de la classe Image.

### affichage

Affiche dans une fenêtre SDL2 une instance de la classe Image.

#### Contrôle de l'affichage

Appuyez sur `escape` pour soritr du programme.
Utilisez les touche `UP` pour zoomer et `DOWN` pour dézommer.
