#include "Image.h"

#include <cassert>
#include <fstream>
#include <iostream>

using namespace std;

Image::Image(){
    dimx = 0;
    dimy = 0;

    //tab initialisé à nullptr pour ne pas allouer de mémoire et ne pas delete
    //un tab non initialisé dans le destructeur (valgrind)
    tab = nullptr;
}

Image::Image(const int &dimensionX, const int &dimensionY){
    dimx = dimensionX;
    dimy = dimensionY;

    int tailleTab = dimx * dimy;

    tab = new Pixel[tailleTab];

    for(unsigned int i = 0; i < tailleTab; i++){
        tab[i] = Pixel();
    }    
}

Image::~Image(){
    dimx = 0;
    dimy = 0;

    delete [] tab;
    tab = nullptr;
}

int Image::getDimX() const{
    return dimx;
}

int Image::getDimY() const{
    return dimy;
}

Pixel& Image::getPix(int x,int y){
    if (x >= 0 && x < dimx && y >= 0 && y < dimy) {
        return tab[y * dimx + x];
    } else {
        // Gérer l'erreur selon les besoins, ici retourne le pixel (0,0)
        return tab[0];
    }
}
    

Pixel Image::getPix (int x,int y) const{
    if (x >= 0 && x < dimx && y >= 0 && y < dimy) {
        return tab[y * dimx + x];
    } else {
        // Gérer l'erreur selon les besoins, ici retourner un pixel noir
        return Pixel();
    }
}

void Image::setPix(int x, int y, const Pixel &p){
    if (x >= 0 && x < dimx && y >= 0 && y < dimy) {
        tab[y * dimx + x] = p;
    }
}

void Image::dessinerRectangle(int minX, int minY, int maxX, int maxY, const Pixel& p){
    for(int i = minY; i <= maxY; i++){
        for(int j = minX; j <= maxX; j++){
            setPix(i, j, p);
        }
    }
}

void Image::effacer(const Pixel &p){
    dessinerRectangle(0, 0, dimx, dimy, p);
}


void Image::sauver(const string &filename) const{
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            fichier << +getPix(x, y).r << " " << +getPix(x, y).g << " " << +getPix(x, y).b << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string &filename)
{
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            fichier >> r >> b >> g;
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole()
{
    cout << dimx << " " << dimy << endl;
    for (unsigned int y = 0; y < dimy; ++y)
    {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel &pix = getPix(x, y);
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}

void Image::testRegression(){
    Image * img = new Image();
    Image * img2 = new Image(100, 100);

    assert(img->dimx == 0);
    assert(img->dimy == 0);
    assert(img->tab == nullptr);

    assert(img2->dimx == 100);
    assert(img2->dimy == 100);

    img2->setPix(10, 10, Pixel(230, 5, 105));
    assert(img2->getPix(10, 10).r == Pixel(230, 5, 105).r);
    assert(img2->getPix(10, 10).g == Pixel(230, 5, 105).g);
    assert(img2->getPix(10, 10).b == Pixel(230, 5, 105).b);
    img2->setPix(10, 10, Pixel());

    img2->dessinerRectangle(0, 0, 100, 100, Pixel(50, 150, 250));
     for(int i = 0; i <= 100; i++){
        for(int j = 0; j <= 100; j++){
            assert(img2->getPix(i, j).r == Pixel(50, 150, 250).r);
            assert(img2->getPix(i, j).g == Pixel(50, 150, 250).g);
            assert(img2->getPix(i, j).b == Pixel(50, 150, 250).b);
        }
     }
    
    img2->effacer(Pixel());
    for(int i = 0; i <= 100; i++){
        for(int j = 0; j <= 100; j++){
            assert(img2->getPix(i, j).r == Pixel().r);
            assert(img2->getPix(i, j).g == Pixel().g);
            assert(img2->getPix(i, j).b == Pixel().b);
        }
     }

    delete img;
    delete img2;
}