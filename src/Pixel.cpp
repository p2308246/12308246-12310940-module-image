#include "Pixel.h"
#include <iostream>

using namespace std;

Pixel::Pixel(){
    r = 0;
    g = 0;
    b = 0;
};

Pixel::Pixel(int nr, int ng, int nb){
    r = nr;
    g = ng;
    b = nb;
};
