#include "ImageViewer.h"

#include <iostream>

using namespace std;

ImageViewer::ImageViewer() : window(nullptr), renderer(nullptr){
    cout<<"SDL: init"<<endl;
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;SDL_Quit();exit(1);
    }

    window = SDL_CreateWindow("Image", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, h, w, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; SDL_Quit(); exit(1);
    }

    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    cout<<"SDL_image: init"<<endl;
    if( !(IMG_Init(imgFlags) & imgFlags)) {
        cout << "SDL_m_image could not initialize! SDL_m_image Error: " << IMG_GetError() << endl;SDL_Quit();exit(1);
    }

    renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}

ImageViewer::~ImageViewer(){
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void ImageViewer::afficher(const Image& im){   
    im.sauver("./data/image.ppm");

    SDL_Surface* imageSurface = IMG_Load("./data/image.ppm");
    if (imageSurface == nullptr) {
        std::cerr << "Échec de chargement de l'image: " << SDL_GetError() << std::endl;
        SDL_DestroyWindow(window);
        SDL_Quit();
    }

    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, imageSurface);
    if (texture == nullptr) {
        std::cerr << "Échec de création de la texture: " << SDL_GetError() << std::endl;
        SDL_FreeSurface(imageSurface);
        SDL_DestroyWindow(window);
        SDL_Quit();
    }

    float zoom = 1.f;
    SDL_Rect destRect;
    destRect.w = imageSurface->w;
    destRect.h = imageSurface->h;
    destRect.x = (w - destRect.w) / 2;
    destRect.y = (h - destRect.h) / 2;

    SDL_Event event;
    bool quit = false;
    while (!quit) {
        while (SDL_PollEvent(&event) != 0) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                quit = true;
            }else if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym) {
                case SDLK_UP:
                    zoom += 0.1;
                    destRect.w = imageSurface->w * zoom;
                    destRect.h = imageSurface->h * zoom;
                    if(destRect.w >= w || destRect.h >= h){
                        zoom -= 0.1;
                        destRect.w = imageSurface->w * zoom;
                        destRect.h = imageSurface->h * zoom;
                    }
                    break;
                case SDLK_DOWN:
                    zoom -= 0.1;
                    if (zoom < 0.1) {
                        zoom = 0.1;
                    }
                    destRect.w = imageSurface->w * zoom;
                    destRect.h = imageSurface->h * zoom;
                    break;
                }
            }
        }

        destRect.x = (w - destRect.w) / 2;
        destRect.y = (h - destRect.h) / 2;

        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, texture, nullptr, &destRect);
        SDL_RenderPresent(renderer);
    }

    SDL_FreeSurface(imageSurface);
}