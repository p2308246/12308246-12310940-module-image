#ifndef PIXEL_H
#define _PIXEL_H
#endif

/**
 * @struct Pixel
 * @brief Définition de la strucutre d'un pixel
*/
struct Pixel
{
    unsigned char r,g,b; ///< couleur du pixel au format (r,g,b)

    /**
     * @brief constructeur du Pixel de couleur (0,0,0)
    */
    Pixel();

    /**
     * @brief constructeur du Pixel de couleur spécifié
     * 
     * @param nr valeur de rouge du pixel
     * @param ng valeur de vert du pixel
     * @param nb valeur de bleu du pixel
    */
    Pixel(int nr, int ng, int nb);

};

