#ifndef IMAGEVIEWER_H
#define _IMAGEVIEWER_H
#endif

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Image.h"

/**
 * @class ImageViewer
 * 
 * @brief Affichage d'une instance de la classe Image
*/
class ImageViewer{
    private:
    SDL_Window * window; ///< Fenêtre de l'affichage
    SDL_Renderer * renderer; ///< Affiche dans la fenêtre
    const unsigned int h = 200; ///< Hauteur de la fenêtre
    const unsigned int w = 200; ///< Largeur de la fenêtre

    public:
    /**
     * @brief constructeur sans paramètre de la classe ImageViewer.
     * 
     * Inistialise le SDL_Window et le SDL_Renderer
    */
    ImageViewer();

    /**
     * @brief Destructeur
    */
    ~ImageViewer();

    /**
     * @brief Affiche l'instance de la classe Image passé en paramètre
     * 
     * @param im Image à afficher.
    */
    void afficher(const Image& im);
};