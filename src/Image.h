#ifndef IMAGE_H
#define _IMAGE_H
#endif

#include "Pixel.h"
#include <fstream>

using namespace std;

/**
 * @class Image
 * @brief classe pour initialiser et modifier une image de (dimx, dimy) pixels
*/
class Image{
    private:
    Pixel * tab; ///< tableau a une dimension des pixels de l'image
    int dimx, dimy; ///< dimension en pixel de l'image donnée par (dimx, dimy)

    public:

    /**
     * @brief constructeur Image sans paramètre, image de dimension 0 sans pixel
    */
    Image();

    /**
     * @brief constructeur Image avec les dimensions en paramètre, tableau initialiser avec des pixels noir.
     * 
     * @param dimensionX Hauteur dimx de l'image
     * @param dimensionY Largeur dimy de l'image
    */
    Image(const int &dimensionX, const int &dimensionY);

    /**
     * @brief Destructeur de la class Image, image de dimension 0 et suppression du tableau
    */
    ~Image();

    /**
     * @brief Donne la dimension dimx de l'image
     * 
     * @return int dimx
    */
    int getDimX() const;

    /**
     * @brief Donne la dimension dimy de l'image
     * 
     * @return int dimy
    */
    int getDimY() const;

    /**
     * @brief geter pour un pixel de l'image
     * 
     * @param x Coordonnée x du pixel
     * @param y Coordonnée y du pixel
     * @return Retourne le pixel du coordonnée (x, y)
    */
    Pixel& getPix(int x, int y);

    /**
     * @brief geter pour une copie d'un pixel de l'image
     * 
     * @param x Coordonnée x du pixel
     * @param y Coordonnée y du pixel
     * @return Retourne une copie du pixel de coordonnée (x, y)
    */
    Pixel getPix(int x, int y) const;

    /**
     * @brief seter d'un pixel dans l'image
     * 
     * @param x Coordonnée x du pixel à remplacer
     * @param y Coordonnée y du pixel à remplacer
     * @param p Pixel à ajouter dans l'image
    */
    void setPix(int x, int y, const Pixel &p);

    /**
     * @brief dessine un rectangle de dimension donnée sur l'image
     * 
     * @param Xmin Coordonnée x de l'angle haut-gauche du rectangle
     * @param Ymin Coordonnée y de l'angle haut-gauche du rectangle
     * @param Xmax Coordonnée x de l'angle bas-droit du rectangle
     * @param Ymax Coordonnée y de l'angle bas-droit du rectangle
    */
    void dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, const Pixel &couleur);

    /**
     * @brief remplie l'image de la couleur spécifiée
     * 
     * @param couleur Pixel avec lequel remplir l'image
    */
    void effacer(const Pixel &couleur);

    /**
     * @brief Sauvegarde l'image dans un fichier créé au format .ppm
     * 
     * @param filename Nom du fichier de l'image sauvegarder
    */
    void sauver(const string &filename) const;

    /**
     * @brief Ouvre un fichier dans une instance d'image
     * 
     * @param filename Nom du fichier de l'image à ouvrir
    */
    void ouvrir(const string &filename);

    /**
     * @brief Affiche dans la console les valeurs des pixels de l'image
    */
    void afficherConsole();

    /**
     * @brief test de la classe
    */
    static void testRegression();
};